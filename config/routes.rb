Rails.application.routes.draw do
  devise_for :userauths 
  resources :probeconfigids do
    collection do
      get :index
      get :show 
      get :new
      get :edit
      post :newprobeconfigid
      patch :update
      put :update
      delete :destroy
    end
  end
  resources :iterations do
    collection do
      get :index
      get :show 
      get :new
      get :edit
      post :newiteration
      patch :update
      put :update
      delete :destroy
    end
  end
  resources :dot_probes do
    collection do
      get :index
      get :show 
      get :new
      get :edit
      post :newdot_probe
      patch :update
      put :update
      delete :destroy
    end
  end
  resources :answers do
    collection do
      get :index
      get :show 
      get :new
      get :edit
      post :newanswer
      patch :update
      put :update
      delete :destroy
    end
  end
  resources :surveys do
    collection do
      get :index
      get :show 
      get :new
      get :edit
      post :newsurvey
      patch :update
      put :update
      delete :destroy
    end
  end
  resources :consents do
    collection do
      get :index
      get :show 
      get :new
      get :edit
      post :newconsent
      patch :update
      put :update
      delete :destroy
    end
  end
  resources :users do
    collection do
      get :index
      get :show 
      get :new
      get :edit
      post :newuser
      patch :update
      put :update
      delete :destroy
    end
  end


=begin
  get "/users" index
  post "/users" create 
  delete "/users" delete
  get "/users/:id" show
  get "/users/new" new 
  get "/users/:id/edit" edit
  patch "/users/:id" update
  put "/users/:id" update
=end



  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'users#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
