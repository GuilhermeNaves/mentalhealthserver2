# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151103023249) do

  create_table "answers", force: :cascade do |t|
    t.integer  "survey_id",    limit: 4
    t.string   "question",     limit: 255
    t.string   "answer",       limit: 255
    t.string   "elapsed_time", limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "answers", ["survey_id"], name: "survey_id", using: :btree

  create_table "consents", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "pdf",        limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "dot_probes", force: :cascade do |t|
    t.integer  "user_id",     limit: 4
    t.string   "start_date",  limit: 255
    t.string   "finish_date", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "iterations", force: :cascade do |t|
    t.integer  "dot_probe_id",   limit: 4
    t.string   "img_left",       limit: 255
    t.string   "img_right",      limit: 255
    t.string   "elapsed_time",   limit: 255
    t.string   "touch_position", limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "probeconfigids", force: :cascade do |t|
    t.string   "iterationId",    limit: 255
    t.string   "imageLeftName",  limit: 255
    t.string   "imageRightName", limit: 255
    t.string   "probePosition",  limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "surveys", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "start_date", limit: 255
    t.string   "end_date",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "userauths", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "login",                  limit: 255
    t.string   "name",                   limit: 255
    t.string   "date_of_birth",          limit: 255
    t.string   "gender",                 limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "userauths", ["email"], name: "index_userauths_on_email", unique: true, using: :btree
  add_index "userauths", ["reset_password_token"], name: "index_userauths_on_reset_password_token", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "login",         limit: 255
    t.string   "password",      limit: 255
    t.string   "email",         limit: 255
    t.string   "name",          limit: 255
    t.string   "date_of_birth", limit: 255
    t.string   "gender",        limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_foreign_key "answers", "surveys", name: "answers_ibfk_1"
  add_foreign_key "consents", "users", column: "id", name: "consents_ibfk_1"
  add_foreign_key "dot_probes", "users", column: "id", name: "dot_probes_ibfk_1"
  add_foreign_key "iterations", "dot_probes", column: "id", name: "iterations_ibfk_1"
  add_foreign_key "surveys", "users", column: "id", name: "surveys_ibfk_1"
end
