class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.integer :survey_id
      t.string :question
      t.string :answer
      t.string :elapsed_time

      t.timestamps null: false
    end
  end
end
