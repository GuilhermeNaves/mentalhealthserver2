class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys do |t|
      t.integer :user_id
      t.string :start_date
      t.string :end_date

      t.timestamps null: false
    end
  end
end
