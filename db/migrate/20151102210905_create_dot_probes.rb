class CreateDotProbes < ActiveRecord::Migration
  def change
    create_table :dot_probes do |t|
      t.integer :user_id
      t.string :start_date
      t.string :finish_date

      t.timestamps null: false
    end
  end
end
