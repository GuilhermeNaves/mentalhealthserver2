class CreateConsents < ActiveRecord::Migration
  def change
    create_table :consents do |t|
      t.integer :user_id
      t.string :pdf

      t.timestamps null: false
    end
  end
end
