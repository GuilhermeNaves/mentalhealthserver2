class CreateIterations < ActiveRecord::Migration
  def change
    create_table :iterations do |t|
      t.integer :dot_probe_id
      t.string :img_left
      t.string :img_right
      t.string :elapsed_time
      t.string :touch_position

      t.timestamps null: false
    end
  end
end
