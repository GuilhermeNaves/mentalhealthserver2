class CreateProbeconfigids < ActiveRecord::Migration
  def change
    create_table :probeconfigids do |t|
      t.string :iterationId
      t.string :imageLeftName
      t.string :imageRightName
      t.string :probePosition

      t.timestamps null: false
    end
  end
end
