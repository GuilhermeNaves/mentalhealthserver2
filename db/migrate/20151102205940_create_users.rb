class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :login
      t.string :password
      t.string :email
      t.string :name
      t.string :date_of_birth
      t.string :gender

      t.timestamps null: false
    end
  end
end
