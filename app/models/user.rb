class User < ActiveRecord::Base
	has_many :consents 
	has_many :dot_probes
	has_many :surveys
end
