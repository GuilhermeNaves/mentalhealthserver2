json.array!(@consents) do |consent|
  json.extract! consent, :id, :user_id, :pdf
  json.url consent_url(consent, format: :json)
end
