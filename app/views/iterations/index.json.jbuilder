json.array!(@iterations) do |iteration|
  json.extract! iteration, :id, :dot_probe_id, :img_left, :img_right, :elapsed_time, :touch_position
  json.url iteration_url(iteration, format: :json)
end
