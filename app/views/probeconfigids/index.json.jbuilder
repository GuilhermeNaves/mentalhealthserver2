json.array!(@probeconfigids) do |probeconfigid|
  json.extract! probeconfigid, :id, :iterationId, :imageLeftName, :imageRightName, :probePosition
  json.url probeconfigid_url(probeconfigid, format: :json)
end
