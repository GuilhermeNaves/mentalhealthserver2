json.array!(@users) do |user|
  json.extract! user, :id, :login, :password, :email, :name, :date_of_birth, :gender
  json.url user_url(user, format: :json)
end
