json.array!(@answers) do |answer|
  json.extract! answer, :id, :survey_id, :question, :answer, :elapsed_time
  json.url answer_url(answer, format: :json)
end
