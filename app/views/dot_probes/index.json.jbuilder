json.array!(@dot_probes) do |dot_probe|
  json.extract! dot_probe, :id, :user_id, :start_date, :finish_date
  json.url dot_probe_url(dot_probe, format: :json)
end
