class ProbeconfigidsController < ApplicationController
  before_action :set_probeconfigid, only: [:show, :edit, :update, :destroy]

  # GET /probeconfigids
  # GET /probeconfigids.json
  def index
    @probeconfigids = Probeconfigid.all
  end

  # GET /probeconfigids/1
  # GET /probeconfigids/1.json
  def show
  end

  # GET /probeconfigids/new
  def new
    @probeconfigid = Probeconfigid.new
  end

  # GET /probeconfigids/1/edit
  def edit
  end

  # POST /probeconfigids
  # POST /probeconfigids.json
  def newprobeconfigid
    @probeconfigid = Probeconfigid.new(probeconfigid_params)

    respond_to do |format|
      if @probeconfigid.save
        format.html { redirect_to @probeconfigid, notice: 'Probeconfigid was successfully created.' }
        format.json { render :show, status: :created, location: @probeconfigid }
      else
        format.html { render :new }
        format.json { render json: @probeconfigid.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /probeconfigids/1
  # PATCH/PUT /probeconfigids/1.json
  def update
    respond_to do |format|
      if @probeconfigid.update(probeconfigid_params)
        format.html { redirect_to @probeconfigid, notice: 'Probeconfigid was successfully updated.' }
        format.json { render :show, status: :ok, location: @probeconfigid }
      else
        format.html { render :edit }
        format.json { render json: @probeconfigid.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /probeconfigids/1
  # DELETE /probeconfigids/1.json
  def destroy
    @probeconfigid.destroy
    respond_to do |format|
      format.html { redirect_to probeconfigids_url, notice: 'Probeconfigid was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_probeconfigid
      @probeconfigid = Probeconfigid.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def probeconfigid_params
      params.require(:probeconfigid).permit(:iterationId, :imageLeftName, :imageRightName, :probePosition)
    end
end
