class DotProbesController < ApplicationController
  before_action :set_dot_probe, only: [:show, :edit, :update, :destroy]

  # GET /dot_probes
  # GET /dot_probes.json
  def index
    @dot_probes = DotProbe.all
  end

  # GET /dot_probes/1
  # GET /dot_probes/1.json
  def show
  end

  # GET /dot_probes/new
  def new
    @dot_probe = DotProbe.new
  end

  # GET /dot_probes/1/edit
  def edit
  end

  # POST /dot_probes
  # POST /dot_probes.json
  def newdot_probe
    @dot_probe = DotProbe.new(dot_probe_params)

    respond_to do |format|
      if @dot_probe.save
        format.html { redirect_to @dot_probe, notice: 'Dot probe was successfully created.' }
        format.json { render :show, status: :created, location: @dot_probe }
      else
        format.html { render :new }
        format.json { render json: @dot_probe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dot_probes/1
  # PATCH/PUT /dot_probes/1.json
  def update
    respond_to do |format|
      if @dot_probe.update(dot_probe_params)
        format.html { redirect_to @dot_probe, notice: 'Dot probe was successfully updated.' }
        format.json { render :show, status: :ok, location: @dot_probe }
      else
        format.html { render :edit }
        format.json { render json: @dot_probe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dot_probes/1
  # DELETE /dot_probes/1.json
  def destroy
    @dot_probe.destroy
    respond_to do |format|
      format.html { redirect_to dot_probes_url, notice: 'Dot probe was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dot_probe
      @dot_probe = DotProbe.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dot_probe_params
      params.require(:dot_probe).permit(:user_id, :start_date, :finish_date)
    end
end
