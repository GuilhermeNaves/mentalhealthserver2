require 'test_helper'

class ProbeconfigidsControllerTest < ActionController::TestCase
  setup do
    @probeconfigid = probeconfigids(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:probeconfigids)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create probeconfigid" do
    assert_difference('Probeconfigid.count') do
      post :create, probeconfigid: { imageLeftName: @probeconfigid.imageLeftName, imageRightName: @probeconfigid.imageRightName, iterationId: @probeconfigid.iterationId, probePosition: @probeconfigid.probePosition }
    end

    assert_redirected_to probeconfigid_path(assigns(:probeconfigid))
  end

  test "should show probeconfigid" do
    get :show, id: @probeconfigid
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @probeconfigid
    assert_response :success
  end

  test "should update probeconfigid" do
    patch :update, id: @probeconfigid, probeconfigid: { imageLeftName: @probeconfigid.imageLeftName, imageRightName: @probeconfigid.imageRightName, iterationId: @probeconfigid.iterationId, probePosition: @probeconfigid.probePosition }
    assert_redirected_to probeconfigid_path(assigns(:probeconfigid))
  end

  test "should destroy probeconfigid" do
    assert_difference('Probeconfigid.count', -1) do
      delete :destroy, id: @probeconfigid
    end

    assert_redirected_to probeconfigids_path
  end
end
