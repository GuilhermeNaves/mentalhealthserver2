require 'test_helper'

class ConsentsControllerTest < ActionController::TestCase
  setup do
    @consent = consents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:consents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create consent" do
    assert_difference('Consent.count') do
      post :create, consent: { pdf: @consent.pdf, user_id: @consent.user_id }
    end

    assert_redirected_to consent_path(assigns(:consent))
  end

  test "should show consent" do
    get :show, id: @consent
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @consent
    assert_response :success
  end

  test "should update consent" do
    patch :update, id: @consent, consent: { pdf: @consent.pdf, user_id: @consent.user_id }
    assert_redirected_to consent_path(assigns(:consent))
  end

  test "should destroy consent" do
    assert_difference('Consent.count', -1) do
      delete :destroy, id: @consent
    end

    assert_redirected_to consents_path
  end
end
