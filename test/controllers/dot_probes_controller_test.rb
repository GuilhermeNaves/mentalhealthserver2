require 'test_helper'

class DotProbesControllerTest < ActionController::TestCase
  setup do
    @dot_probe = dot_probes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dot_probes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dot_probe" do
    assert_difference('DotProbe.count') do
      post :create, dot_probe: { finish_date: @dot_probe.finish_date, start_date: @dot_probe.start_date, user_id: @dot_probe.user_id }
    end

    assert_redirected_to dot_probe_path(assigns(:dot_probe))
  end

  test "should show dot_probe" do
    get :show, id: @dot_probe
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dot_probe
    assert_response :success
  end

  test "should update dot_probe" do
    patch :update, id: @dot_probe, dot_probe: { finish_date: @dot_probe.finish_date, start_date: @dot_probe.start_date, user_id: @dot_probe.user_id }
    assert_redirected_to dot_probe_path(assigns(:dot_probe))
  end

  test "should destroy dot_probe" do
    assert_difference('DotProbe.count', -1) do
      delete :destroy, id: @dot_probe
    end

    assert_redirected_to dot_probes_path
  end
end
